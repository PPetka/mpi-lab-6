#pragma once
//self defined
#include "Argument.h"
#include "Process.h"
#include "Pack.h"
#include "Matrix.h"
#include "Solution.h"



//external
#include"mpi.h"

//standard
#include <vector>
#include <iostream>
#include <fstream>



using namespace std;
class Master : public Process {

private: string pathA;
private: string pathB;
private: string pathC;
private: Matrix *m1;
private: Matrix *m2;

public: Master(int worldSize, Argument **args, int argSize);

public:	void start();

private: Matrix *fileToMatrix(string fileName);
private: void matrixToFile(Matrix *matrix, string text);
private: bool multiplyPassibility();

private: vector<int*> *possibleBoxes(int number);
private: vector<Solution*> *findSolutions();
private: Solution* chooseOptimumSolution(vector<Solution*> *vec);
private: bool test(int x, int y, int sqX, int sqY, int sqAmmoutX, int sqAmmoutY, int worldSize);
private: vector<Package*> *devideMatrixToPackages(Solution *solution);
private: Matrix *gatherMatrix(vector<Matrix*>,Solution &solution);
private: void sendExitMessage(); 
};
