#include "Process.h"

void Process::start()
{
	std::cout << "this method must be overwriten" << std::endl;
}
char * Process::reciveMessage()
{
	MPI_Status status;
	MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	int ammount;
	MPI_Get_count(&status, MPI_CHAR, &ammount);
	//std::cout << "ammount " << ammount << std::endl;

	char *buffer = new char[ammount];
	MPI_Recv(buffer, ammount, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	return (char*)buffer;
}

Matrix * Process::multiply(Matrix * pA, Matrix * pB)
{
	double **matrix = new double*[pA->getY()];
	for (int i = 0; i < pA->getY(); i++)
		matrix[i] = new double[pB->getX()];
	
	//
	for (int i = 0; i < pA->getY(); i++) {
		for (int j = 0; j < pB->getX(); j++) {

			int val = 0;
			for (int c = 0; c < pA->getX(); c++) {
				val += pA->matrix[i][c] * pB->matrix[c][j];
			}
			matrix[i][j] = val;
		}
	}
	return new Matrix(matrix, pB->getX(), pA->getY());
}
