#pragma once
#include "Process.h"
#include "Pack.h"

#include "mpi.h"

using namespace std;
class Slave : public Process
{
public: Slave(int procId, int worldSize) : Process(procId, worldSize) {};

public: void start();
};