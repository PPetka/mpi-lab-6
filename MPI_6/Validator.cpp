#include "Validator.h"

using namespace std;
Validator::Validator(int argc, char ** argv) {
	this->argc = argc;
	this->argv = argv;
}

void Validator::addArgument(Argument * arg){
	arguments.push_back(arg);
}

void Validator::addArgument(Argument ** arg, int arrSize)
{
	for (int i = 0; i < arrSize; i++) {
		arguments.push_back(arg[i]); 
	}
}

bool Validator::validate() {
	if ((argc - 1) != arguments.size() * 2) {
		cout << "wrong number of arguments have been passed, required " << arguments.size() << " argument(s)." << endl;
		return false;
	}
	//checking if every expected command has been pased
	for each (auto arg in arguments) {
		string argCommand(arg->getCommandName());
		//passed commands
		for (int i = 1; i < argc; i += 2) {
			string consoleInputCommand(argv[i]);
			//if consoleInputCommand command matches expected command
			if (argCommand.compare(consoleInputCommand) == 0) {
				arg->commandCorrecntess = true; 
				switch (arg->getInputType()) {
				case Argument::ARG_INTEGER:
					arg->typeCorrecntess = is<int>(argv[i + 1]);
					break;
				case  Argument::ARG_CHAR:
					arg->typeCorrecntess = is<char>(argv[i + 1]);
					break;
				case Argument::ARG_STRING:
					arg->typeCorrecntess = is<std::string>(argv[i + 1]);
					break;
				case Argument::ARG_BOOLEAN:
					arg->typeCorrecntess = is<bool>(argv[i + 1]);
					break;
				case Argument::ARG_DOUBLE:
					arg->typeCorrecntess = is<double>(argv[i + 1]);
					break;
				case Argument::ARG_FLOAT:
					arg->typeCorrecntess = is<float>(argv[i + 1]);
					break;
				case Argument::ARG_SHORT:
					arg->typeCorrecntess = is<short>(argv[i + 1]);
					break;
				}
			}//if type is correct set value
			if (arg->typeCorrecntess) {
				arg->setValue(argv[i + 1]);
				break;
			}
		}
	
		if (!arg->commandCorrecntess || !arg->typeCorrecntess) {
			cout <<endl<< arg->getErrors() << endl; 
			return false; 
		}
	}
	return true;
}

void Validator::clear() {
	arguments.clear();
}

string Validator::getArgumentHelpInfo() {
	stringstream ss;
	ss << "program require " << arguments.size() << " arguments:" << endl;
	int argCounter = 1;
	for each (auto arg in arguments)
	{
		ss << argCounter << ") commandName " << arg->getCommandName() << "; describtion: " << arg->getDescribtion() << "; argumentType: " << arg->enumToString(arg->getInputType()) << ";" << endl;
		argCounter++;
	}
	return ss.str();
}


