#pragma once
#include <iostream>
#include <string>

class Argument {

public: static enum CommandType {
	ARG_INTEGER,
	ARG_DOUBLE,
	ARG_FLOAT,
	ARG_SHORT,
	ARG_CHAR,
	ARG_BOOLEAN,
	ARG_STRING
};

private: char *commandName;
private: int inputType;
private: char *describtion;

private: char *value; 

public: bool commandCorrecntess;
public: bool typeCorrecntess;

public: Argument(char *commandName, int inputType, char*describtion);

public: std::string getErrors();

public: char * enumToString(int enumVal) {
	switch (enumVal) {
	case ARG_INTEGER:
		return "int";
	case ARG_DOUBLE:
		return "double";
	case ARG_FLOAT:
		return "float";
	case ARG_SHORT:
		return "short int";
	case ARG_CHAR:
		return "char";
	case ARG_BOOLEAN:
		return "bool";
	case ARG_STRING:
		return "string";
	default:
		return "";
		break;
	}
}
//getters
public: char* getCommandName() {
	return this->commandName;
}
public: int getInputType() {
	return this->inputType;
}

public: char * getDescribtion() {
	return describtion; 
}

public: char *getValue() { return value; }

//setters
public:void setValue(char *val) {
	this->value = val; 
}


};