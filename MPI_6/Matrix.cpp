#include "Matrix.h"


Matrix::Matrix(char * buffer) : Matrix((double*)(buffer + (2 * sizeof(int))), (int)buffer[0], (int)buffer[sizeof(int)]) {}

Matrix::Matrix(double * matrix, int sizeX, int sizeY) {
	this->x = sizeX;
	this->y = sizeY;
	this->packSizeInBytes = (x*y) * sizeof(double) + (2 * (int)sizeof(int));

	//allocation
	this->matrix = new double*[sizeY];
	for (int i = 0; i < sizeY; i++)
		this->matrix[i] = new double[sizeX];

	//convertion
	int iterator = 0;
	for (int i = 0; i < sizeY; i++) {
		for (int j = 0; j < sizeX; j++) {
			this->matrix[i][j] = matrix[iterator];
			iterator++;
		}
	}
}

Matrix::Matrix(double ** matrix, int sizeX, int sizeY){
	this->x = sizeX;
	this->y = sizeY;
	this->packSizeInBytes = (x*y) * sizeof(double) + (2 * (int)sizeof(int));

	this->matrix = matrix;
}


Matrix::~Matrix() {
	for (int i = 0; i < this->x; i++)
		delete[] this->matrix[i];
	delete[] this->matrix;
}

void * Matrix::pack(){
	char *buffer = new char[packSizeInBytes];
	int *temp = (int*)buffer;
	*temp = x; temp++;
	*temp = y; temp++;
	double *temp2 = (double*)temp;

	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++) {
			*temp2 = matrix[i][j];
			temp2++;
		}
	}
	return (void*)buffer;
}

double * Matrix::getPart(int startX, int startY, int sqX, int sqY){
	double *buffer = new double[sqX*sqY];
	int iterator = 0;
	for (int i = startY; i < startY + sqY; i++) {
		for (int j = startX; j < startX + sqX; j++) {
			buffer[iterator] = this->matrix[i][j];
			iterator++;
		}
	}
	return buffer;
}

void Matrix::display(){
	for (int i = 0; i < y; i++) {
		if (i > 0)
			std::cout << std::endl;
		for (int j = 0; j < x; j++) {
			std::cout << this->matrix[i][j] << " ";
		}
	}
	cout << "\n\n";

}
