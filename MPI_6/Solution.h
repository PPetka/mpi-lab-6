
class Solution {
public:
	int x;
	int y;
	int sqX;
	int sqY;
	int sqAmmX;
	int sqAmmY;

private: int pointsPerProc;

public: Solution(int x, int y, int sqX, int sqY, int sqAmmoutX, int sqAmmoutY) {
	this->x = x;
	this->y = y;
	this->sqX = sqX;
	this->sqY = sqY;
	this->sqAmmX = sqAmmoutX;
	this->sqAmmY = sqAmmoutY;

	this->pointsPerProc = 0;
}
//setters;
public: void setPointsPerProc(int ppp) {
	this->pointsPerProc = ppp;
}


		//getters
public: int getPointsPerProc() { return pointsPerProc; };
};