#pragma once
/*represents matrix as rectangle buffer (2dimentional array) */
#include <iostream>
using namespace std;
class Matrix {

protected: int x;
protected: int y;
public: int packSizeInBytes;

public: double **matrix;

public: Matrix(char *buffer);
public: Matrix(double *matrix, int sizeX, int sizeY);
public: Matrix(double **matrix, int sizeX, int sizeY);

public: ~Matrix();
public: void *pack();
public: double *getPart(int startX, int startY, int sqX, int sqY);
public: void display();

public: int getX() { return x; }
public: int getY() { return y; }
};