#include "mpi.h"
#include <iostream>
#include "Process.h"
#include "Master.h"
#include "Slave.h"
#include "Validator.h"

using namespace std;

void my_swap(vector<int> &vec)
{
	for (int i = 0; i < vec.size(); i++)
		vec.at(i) = 4;
}
int main(int argc, char **argv) {
	Validator *validator = new Validator(argc, argv);

	MPI_Init(&argc, &argv);
	int procID, worldSize;
	MPI_Comm_rank(MPI_COMM_WORLD, &procID);
	MPI_Comm_size(MPI_COMM_WORLD, &worldSize);


	Argument **args = new Argument*[3];
	args[0] = new Argument("-pathA", Argument::CommandType::ARG_STRING, "path to first matrix required for calculations");
	args[1] = new Argument("-pathB", Argument::CommandType::ARG_STRING, "path to second matrix required for calculations");
	args[2] = new Argument("-pathC", Argument::CommandType::ARG_STRING, "path for output matrix");

	validator->addArgument(args, 3);

	if (validator->validate()) {
		Process *process;
		if (procID == 0) {
			process = new Master(worldSize, args, 3);
		}
		else {
			process = new Slave(procID, worldSize);
		}
		process->start();
	}
	else {
		if (procID == 0)
			cout << endl << validator->getArgumentHelpInfo();
	}
	MPI_Finalize();
}