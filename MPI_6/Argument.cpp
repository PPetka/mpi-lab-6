#include "Argument.h"

Argument::Argument(char * commandName, int inputType,char *describtion){
	this->commandName = commandName;
	this->inputType = inputType; 
	this->describtion = describtion;

	this->typeCorrecntess = false;
	this->commandCorrecntess = false; 
}

std::string Argument::getErrors(){
	std::string errorMessage = "";
	if (!commandCorrecntess)
		errorMessage = std::string("Error\nwrong command(s) name, please write describtion"); 
	else if (!typeCorrecntess)
		errorMessage = std::string("Error\ncommand: ") + std::string(commandName) + " expects type of: " + std::string(enumToString(inputType));

	return errorMessage;
}
