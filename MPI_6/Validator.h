#pragma once
#include <vector>
#include <sstream>
#include "Argument.h"
#include <sstream>
#include <iostream>

using namespace std; 

class Validator {
private: int argc;
private: char** argv;
private: vector<Argument*> arguments; 

public: Validator(int argc, char **argv);

public: void addArgument(Argument *arg);
public: void addArgument(Argument **arg, int arrSize);
public: bool validate();
public: void clear();

private: template <typename T> bool is(char *input) {
	T x;
	std::istringstream iss(input);
	char c;
	return  iss >> x && !(iss >> c);
}

public: string getArgumentHelpInfo();
};


