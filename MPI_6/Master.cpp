#include "Master.h"

Master::Master(int worldSize, Argument ** args, int argSize) : Process(0, worldSize) {
	if (argSize == 3) {
		this->pathA = string(args[0]->getValue());
		this->pathB = string(args[1]->getValue());
		this->pathC = string(args[2]->getValue());
		m1 = NULL;
		m2 = NULL;
	}
	else {
		cout << "wrong number of arguments have been passed" << endl;
	}
};

void Master::start() {
	m1 = this->fileToMatrix(this->pathA);
	m2 = this->fileToMatrix(this->pathB);

	if (multiplyPassibility()) {
		vector<Solution*> *solutions = findSolutions();
		if (!solutions->empty()) {
			Solution *optiomumSolution = chooseOptimumSolution(solutions);
			vector<Package*> *packages = devideMatrixToPackages(optiomumSolution);
			//send
			for (int i = 1; i < packages->size(); i++) {
				void *pck = packages->at(i)->pack();
				if (packages->at(i)->destinityProcess != 0) {
					MPI_Send(pck, packages->at(i)->packSizeInBytes, MPI_BYTE, packages->at(i)->destinityProcess, 666, MPI_COMM_WORLD);
				}
			}
			//calculate process 0 job
			vector<Matrix*> resultMatrixVector;
			Matrix *processZeroCalculation = this->multiply(packages->at(0), packages->at(1));
			processZeroCalculation->display();
			resultMatrixVector.push_back(processZeroCalculation);

			//recive
			for (int i = 0; i < worldSize - 1; i++) {
				char *packedMatrix = this->reciveMessage();
				Matrix * temp = new Matrix(packedMatrix);
				temp->display();
				resultMatrixVector.push_back(temp);
			}

			//glue matrix together
			Matrix *resultMatrix = gatherMatrix(resultMatrixVector, *optiomumSolution);
			resultMatrix->display(); 
			matrixToFile(resultMatrix, this->pathC); 
		}
		else {
			cout << "Can not devide this matrix into " << this->worldSize << " processes. No solutions was found" << endl;
			sendExitMessage();
		}
	}
	else {
		cout << "ERROR, could not multiply matrixs becouse of m1X != m2Y" << endl;
		sendExitMessage();
	}

}

Matrix *Master::fileToMatrix(string fileName) {
	int sizeX = 0;
	int sizeY = 0;
	double *matrix = NULL;
	try {
		std::ifstream stream(fileName);
		stream >> (int)sizeY;
		stream >> (int)sizeX;

		matrix = new double[sizeX*sizeY];
		int iterator = 0;
		while (stream) {
			stream >> matrix[iterator];
			iterator++;
		}
	}
	catch (exception e) {
		cout << "can not read from file: " << e.what() << endl;
	}
	return new Matrix(matrix, sizeX, sizeY);
}

void Master::matrixToFile(Matrix * resultMatrix,string text)
{
	ofstream stream(text);
	stream << resultMatrix->getY() << " " << resultMatrix->getX();

	for (int i = 0; i < resultMatrix->getY(); i++) {
		stream << '\n';
		for (int j = 0; j < resultMatrix->getX(); j++) {		
			stream << resultMatrix->matrix[i][j] << ' ';
		}
	}
}

bool Master::multiplyPassibility() {
	if (m1->getX() == m2->getY())
		return true;

	return false;
}

Solution * Master::chooseOptimumSolution(vector<Solution*>* vec)
{
	//calculating number of boxes needed to be sent for each process by formula:
	// points = pointsA + points B
	// points A = sqY * m1x
	// points B = sqX * m2Y
	for (int i = 0; i < vec->size(); i++) {
		vec->at(i)->setPointsPerProc(vec->at(i)->sqY *m1->getX() + vec->at(i)->sqX *m2->getY());
		//	cout << "solution points " << vec->at(i)->getPointsPerProc() << endl;
	}
	//choosing solution with lowest pointsPerProcess
	int lowestPoints = INT32_MAX;
	int iterationOfBestSolution = -1;
	for (int i = 0; i < vec->size(); i++) {
		if (vec->at(i)->getPointsPerProc() < lowestPoints) {
			lowestPoints = vec->at(i)->getPointsPerProc();
			iterationOfBestSolution = i;
		}
	}
	return vec->at(iterationOfBestSolution);
}

vector<Solution*>* Master::findSolutions() {
	vector<Solution*> *vec = new vector<Solution*>();
	int x = m2->getX();
	int y = m1->getY();

	int squaresPerProc = 0;

	int sqY = 0;
	int sqX = 0;
	int sqAmmoutY = 0;
	int sqAmmoutX = 0;

	//check if there is passibility for dividing matrix at the same ammout of points
	if ((x*y) % worldSize == 0) {
		squaresPerProc = (x*y) / worldSize;

		vector<int*> *boxes = possibleBoxes(squaresPerProc);
		for (int i = 0; i < boxes->size(); i++) {
			sqX = boxes->at(i)[0]; sqY = boxes->at(i)[1];
			// exclude when edges are not suitable 
			if (x % sqX == 0 && y % sqY == 0) {
				sqAmmoutX = x / sqX;
				sqAmmoutY = y / sqY;
				if (test(x, y, sqX, sqY, sqAmmoutX, sqAmmoutY, worldSize))
					vec->push_back(new Solution(x, y, sqX, sqY, sqAmmoutX, sqAmmoutY));
			}
		}
	}
	return vec;
}

vector<int*> *Master::possibleBoxes(int number)
{
	vector<int*> *boxes = new vector<int*>();
	for (int i = 1; i <= number; i++)
		for (int j = 1; j <= number; j++) {
			if (i*j == number)
				boxes->push_back(new int[2]{ i, j });
		}

	return boxes;
}

bool Master::test(int x, int y, int sqX, int sqY, int sqAmmoutX, int sqAmmoutY, int worldSize)
{
	if ((sqAmmoutX * sqAmmoutY) == worldSize && (sqAmmoutX *sqX) == x && (sqAmmoutY * sqY) == y) {
		//cout << " dimensions: " << "x " << x << " y " << y << " sqX " << sqX << " sqAmmX " << sqAmmoutX << " sqY " << sqY << " sqAmmY " << sqAmmoutY << endl;
		return true;
	}

	return	false;
}

vector<Package*>* Master::devideMatrixToPackages(Solution * solution)
{
	vector<Package*>* vectorOfPackages = new vector<Package*>;
	// << "choosed solution with points: " << solution->getPointsPerProc() << endl;

	int destinationProcess = 0;
	for (int i = 0; i < solution->x; i += solution->sqX) {
		for (int j = 0; j < solution->y; j += solution->sqY) {
			//	cout << "i:" << i << " j:" << j << endl;
			vectorOfPackages->push_back(new Package(m1->getPart(0, j, m1->getX(), solution->sqY), m1->getX(), solution->sqY, destinationProcess, 'A'));
			vectorOfPackages->push_back(new Package(m2->getPart(i, 0, solution->sqX, m2->getY()), solution->sqX, m2->getY(), destinationProcess, 'B'));
			destinationProcess++;
		}
	}
	return vectorOfPackages;
}

Matrix * Master::gatherMatrix(vector<Matrix*> vec, Solution &solution)
{
	//allocation
	double **buffer = new double*[solution.y];
	for (int i = 0; i < solution.y; i++)
		buffer[i] = new double[solution.x];

	//convertion
	int vectorCounter = 0;
	for (int i = 0; i < solution.x; i += solution.sqX) {
		for (int j = 0; j < solution.y; j += solution.sqY) {
			//
			for (int y = 0; y < vec.at(vectorCounter)->getY(); y++)
				for (int x = 0; x < vec.at(vectorCounter)->getX(); x++) {
					buffer[j + y][i + x] = vec.at(vectorCounter)->matrix[y][x];
				}
			vectorCounter++;

		}
	}
	return new Matrix(buffer, solution.x, solution.y);
}

void Master::sendExitMessage()
{
	for (int i = 1; i < worldSize; i++) {
		char buff = 'S';
		MPI_Send(&buff, 1, MPI_BYTE, i, 999, MPI_COMM_WORLD);
	}
}




