#include "Pack.h"

Package::Package(char * buffer) : Matrix((double*)(buffer + (2 * sizeof(int)) + 1), (int)buffer[1], (int)buffer[1 + sizeof(int)])
{
	packageSign = *buffer;
}

Package::Package(double * matrix, int sizeX, int sizeY, int dest, char packSign) : Matrix(matrix, sizeX, sizeY) {
	this->destinityProcess = dest;
	this->packageSign = packSign;
	this->packSizeInBytes = (x*y)* sizeof(double) + (2 * (int)sizeof(int)) + 1;

}

//pack class into one space in memory as : char,int,int,*doubleBuffer  -> packageSign, x,y, matrix
void * Package::pack()
{
	char *buffer = new char[packSizeInBytes];

	*buffer = packageSign; char* t = buffer; t++;
	int *temp = (int*)t;
	*temp = x; temp++;
	*temp = y; temp++;
	double *temp2 = (double*)temp;

	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++) {
			*temp2 = matrix[i][j];
			temp2++;
		}
	}

	return (void*)buffer;
}

