#pragma once
#include <iostream>
#include "Matrix.h"
#include "Pack.h"
#include "mpi.h"
class Process
{
protected: int procID;
protected: int worldSize;

public: Process(int procID, int worldSize) {
	this->procID = procID;
	this->worldSize = worldSize;
}
public: virtual void start();

protected: char*reciveMessage();
protected: Matrix *multiply(Matrix *pA, Matrix *pB);
};