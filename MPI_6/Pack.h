#pragma once
#include "Matrix.h"

/*matrix prepared for sending */

class Package :public Matrix {

public: char packageSign;
public: int destinityProcess;

public: Package(char *buffer); 
public: Package(double * matrix, int sizeX, int sizeY, int dest, char packageSign);
public: void *pack();
};